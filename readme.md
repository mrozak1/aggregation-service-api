# aggregation-service-api

## prerequisites

- docker
- jdk 17

## how to run
 
1. clone https://gitlab.com/mrozak1/aggregation-service-api.git repository.
2. change directory to `aggregation-service-api`.
3. run `.\gradlew clean build -x test`.
4. run `docker compose up --build`.

In real life scenario, step 3rd and 4th could be replaced by just `docker compose up` command.
This is because jars and docker images are typically stored in artifactory and hub.

5. After running 4th command, you should see following log messages indicating applications are up and running:
   - Started Application in 3.695 seconds (process running for 5.049)
   - Started BackendServicesApplicationKt in 3.735 seconds (process running for 4.376)
6. Now `aggregation-service-api` is available on `localhost:8080`.

## todo

Due to limited time (8 hours) I did not manage to implement everything as I would on a regular project. Things to add:

- api input validation.
- openapi documentation.
- kubernetes and helm configuration.
- cicd configuration.

## design decisions

- I am fan of hexagonal architecture, I think code written in that ways is more readable and testable hence the decision.
- I am using Webflux so that I can easily execute multiple requests simultaneously.  
- I am using WireMock so that I can easily simulate 200, 503 and timeout scenarios. 
- Because I have some 1to1 mapping I am using MapStruct to make it easier. 
