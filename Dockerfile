FROM eclipse-temurin:17-alpine
COPY build/libs/aggregation-service-api-1.0-SNAPSHOT.jar aggregation-service-api.jar
ENTRYPOINT ["java","-jar","/aggregation-service-api.jar"]
