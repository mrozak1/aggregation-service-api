package org.mrozak.infrastructure.boundaries.inbound.api;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.test.web.reactive.server.WebTestClient;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.mrozak.infrastructure.boundaries.inbound.api.AggregatedDetailsEndpoint.AGGREGATION_ENDPOINT;
import static org.mrozak.infrastructure.boundaries.inbound.api.AggregatedDetailsEndpoint.PRICING_COUNTRY_CODES_QUERY_PARAM;
import static org.mrozak.infrastructure.boundaries.inbound.api.AggregatedDetailsEndpoint.SHIPMENTS_ORDER_NUMBERS_QUERY_PARAM;
import static org.mrozak.infrastructure.boundaries.inbound.api.AggregatedDetailsEndpoint.TRACK_ORDER_NUMBERS_QUERY_PARAM;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@AutoConfigureWireMock(port = 0)
class AggregateAllApisIT {

    @Autowired
    private WebTestClient webTestClient;

    @Test
    void shouldReturnAggregatedResultWithFilledDetails() {
        webTestClient
                .get()
                .uri(builder -> builder
                        .path(AGGREGATION_ENDPOINT)
                        .queryParam(SHIPMENTS_ORDER_NUMBERS_QUERY_PARAM, 83746)
                        .queryParam(PRICING_COUNTRY_CODES_QUERY_PARAM, "PL")
                        .queryParam(TRACK_ORDER_NUMBERS_QUERY_PARAM, 74563)
                        .build())
                .exchange()
                .expectStatus().isEqualTo(200)
                .expectBody(AggregatedDetailsResponse.class).value(result ->
                        assertAll(
                                () -> assertThat(result.shipments()).isNotEmpty(),
                                () -> assertThat(result.track()).isNotEmpty(),
                                () -> assertThat(result.pricing()).isNotEmpty()
                        ));
    }

    @Test
    void shouldNotIncludeDetailsIfApiUnavailableOrTimeout() {
        webTestClient
                .get()
                .uri(builder -> builder
                        .path(AGGREGATION_ENDPOINT)
                        .queryParam(SHIPMENTS_ORDER_NUMBERS_QUERY_PARAM, 4356711, 83746, 123542)
                        .queryParam(PRICING_COUNTRY_CODES_QUERY_PARAM, "PL", "RU", "NL")
                        .queryParam(TRACK_ORDER_NUMBERS_QUERY_PARAM, 23599, 74563, 95212)
                        .build())
                .exchange()
                .expectStatus().isEqualTo(200)
                .expectBody(AggregatedDetailsResponse.class).value(result ->
                        assertAll(
                                () -> assertThat(result.shipments()).containsKey("83746"),
                                () -> assertThat(result.shipments()).doesNotContainKeys("123542", "4356711"),
                                () -> assertThat(result.track()).containsKey("74563"),
                                () -> assertThat(result.track()).doesNotContainKeys("95212", "23599"),
                                () -> assertThat(result.pricing()).containsKey("PL"),
                                () -> assertThat(result.pricing()).doesNotContainKeys("NL", "RU")
                        ));
    }

    @Test
    void shouldReturnAggregatedResultWithoutFilledDetailsIfParamsNotSpecified() {
        webTestClient
                .get()
                .uri(builder -> builder
                        .path(AGGREGATION_ENDPOINT)
                        .build())
                .exchange()
                .expectStatus().isEqualTo(200)
                .expectBody(AggregatedDetailsResponse.class).value(result ->
                        assertAll(
                                () -> assertThat(result.shipments()).isEmpty(),
                                () -> assertThat(result.track()).isEmpty(),
                                () -> assertThat(result.pricing()).isEmpty()
                        ));
    }
}
