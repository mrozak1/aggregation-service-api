package org.mrozak.infrastructure.boundaries.inbound.api;

import org.junit.jupiter.api.Test;
import org.mrozak.domain.AggregatedDetails;
import org.mrozak.domain.pricing.model.Pricing;
import org.mrozak.domain.shipment.model.Shipment;
import org.mrozak.domain.shipment.model.Shipment.Product;
import org.mrozak.domain.track.model.Track;
import org.mrozak.domain.track.model.Track.Status;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import static java.util.Map.entry;
import static org.assertj.core.api.Assertions.assertThat;

class AggregatedDetailsResponseMapperTest {

    private static final AggregatedDetailsResponseMapper AGGREGATED_DETAILS_RESPONSE_MAPPER = new AggregatedDetailsResponseMapper();

    @Test
    void shouldMapToResponse() {
        // given
        final Entry<AggregatedDetails, AggregatedDetailsResponse> domainAndResponse = domainAndResponse();

        // when
        final AggregatedDetailsResponse aggregatedDetailsResponse = AGGREGATED_DETAILS_RESPONSE_MAPPER.toResponse(domainAndResponse.getKey());

        // then
        assertThat(aggregatedDetailsResponse).isEqualTo(domainAndResponse.getValue());
    }

    private Entry<AggregatedDetails, AggregatedDetailsResponse> domainAndResponse() {
        final AggregatedDetails aggregatedDetails = new AggregatedDetails(
                List.of(
                        new Shipment(524523, List.of(Product.ENVELOPE)),
                        new Shipment(856742, List.of())
                ),
                List.of(new Track(7457334, Status.COLLECTED)),
                List.of(new Pricing("NL", BigDecimal.TEN))
        );
        final AggregatedDetailsResponse aggregatedDetailsResponse = new AggregatedDetailsResponse(
                Map.of("524523", List.of("ENVELOPE")),
                Map.of("7457334", "COLLECTED"),
                Map.of("NL", BigDecimal.TEN)
        );
        return entry(aggregatedDetails, aggregatedDetailsResponse);
    }
}
