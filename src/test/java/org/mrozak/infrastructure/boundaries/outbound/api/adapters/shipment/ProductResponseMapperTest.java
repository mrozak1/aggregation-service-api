package org.mrozak.infrastructure.boundaries.outbound.api.adapters.shipment;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mapstruct.factory.Mappers;
import org.mrozak.domain.shipment.model.Shipment;
import org.mrozak.domain.shipment.model.Shipment.Product;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class ProductResponseMapperTest {

    private static final ProductResponseMapper PRODUCT_RESPONSE_MAPPER = Mappers.getMapper(ProductResponseMapper.class);

    @ParameterizedTest
    @CsvSource({
            "ENVELOPE,ENVELOPE",
            "BOX,BOX",
            "PALLET,PALLET"
    })
    void shouldMapToDomain(final ProductResponse productResponse, final Product expectedProduct) {
        // given
        final int orderNumber = 354624;

        // when
        final Shipment shipment = PRODUCT_RESPONSE_MAPPER.toDomain(orderNumber, List.of(productResponse));

        // then
        assertThat(shipment.products()).isEqualTo(List.of(expectedProduct));
        assertThat(shipment.orderNumber()).isEqualTo(orderNumber);
    }
}
