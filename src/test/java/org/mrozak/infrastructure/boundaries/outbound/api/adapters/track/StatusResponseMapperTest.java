package org.mrozak.infrastructure.boundaries.outbound.api.adapters.track;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mapstruct.factory.Mappers;
import org.mrozak.domain.track.model.Track;
import org.mrozak.domain.track.model.Track.Status;

import static org.assertj.core.api.Assertions.assertThat;

class StatusResponseMapperTest {

    private static final StatusResponseMapper STATUS_RESPONSE_MAPPER = Mappers.getMapper(StatusResponseMapper.class);

    @ParameterizedTest
    @CsvSource({
            "NEW,NEW",
            "IN_TRANSIT,IN_TRANSIT",
            "COLLECTING,COLLECTING",
            "COLLECTED,COLLECTED",
            "DELIVERING,DELIVERING",
            "DELIVERED,DELIVERED"
    })
    void shouldMapToDomain(final StatusResponse statusResponse, final Status expectedStatus) {
        // given
        final int orderNumber = 354624;

        // when
        final Track track = STATUS_RESPONSE_MAPPER.toDomain(orderNumber, statusResponse);

        // then
        assertThat(track.status()).isEqualTo(expectedStatus);
        assertThat(track.orderNumber()).isEqualTo(orderNumber);
    }
}
