package org.mrozak.infrastructure.boundaries.outbound.api.adapters.track;

import org.mrozak.domain.track.model.Track;
import org.mrozak.domain.track.ports.TrackApiPort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientRequestException;
import org.springframework.web.reactive.function.client.WebClientResponseException;

import io.netty.handler.timeout.ReadTimeoutException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

@Slf4j
@Component
@RequiredArgsConstructor
class TrackApiAdapter implements TrackApiPort {

    private final WebClient backendServicesWebClient;
    private final StatusResponseMapper statusResponseMapper;

    @Override
    public Mono<Track> getTrack(final int orderNumber) {
        return backendServicesWebClient
                .get()
                .uri(builder -> builder
                        .path("/track-status")
                        .queryParam("orderNumber", orderNumber)
                        .build())
                .retrieve()
                .bodyToMono(StatusResponse.class)
                .onErrorResume(WebClientResponseException.class, exception -> {
                    if (exception.getStatusCode() == HttpStatus.SERVICE_UNAVAILABLE) {
                        log.warn("Track service returned 503 for order number [{}].", orderNumber, exception);
                        return Mono.empty();
                    }
                    return Mono.error(exception);
                })
                .onErrorResume(WebClientRequestException.class, exception -> {
                    if (exception.getCause() instanceof ReadTimeoutException) {
                        log.warn("Track service request timeout for order number [{}].", orderNumber, exception);
                        return Mono.empty();
                    }
                    return Mono.error(exception);
                })
                .map(status -> statusResponseMapper.toDomain(orderNumber, status));
    }
}
