package org.mrozak.infrastructure.boundaries.outbound.api.adapters.pricing;

import org.mrozak.domain.pricing.model.Pricing;
import org.mrozak.domain.pricing.ports.PricingApiPort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientRequestException;
import org.springframework.web.reactive.function.client.WebClientResponseException;

import java.math.BigDecimal;

import io.netty.handler.timeout.ReadTimeoutException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

@Slf4j
@Component
@RequiredArgsConstructor
class PricingApiAdapter implements PricingApiPort {

    private final WebClient backendServicesWebClient;

    @Override
    public Mono<Pricing> getPricing(final String countryCode) {
        return backendServicesWebClient
                .get()
                .uri(builder -> builder
                        .path("/pricing")
                        .queryParam("countryCode", countryCode)
                        .build())
                .retrieve()
                .bodyToMono(BigDecimal.class)
                .onErrorResume(WebClientResponseException.class, exception -> {
                    if (exception.getStatusCode() == HttpStatus.SERVICE_UNAVAILABLE) {
                        log.warn("Pricing service returned 503 for country code [{}].", countryCode, exception);
                        return Mono.empty();
                    }
                    return Mono.error(exception);
                })
                .onErrorResume(WebClientRequestException.class, exception -> {
                    if (exception.getCause() instanceof ReadTimeoutException) {
                        log.warn("Pricing service request timeout for country code [{}].", countryCode, exception);
                        return Mono.empty();
                    }
                    return Mono.error(exception);
                })
                .map(pricing -> new Pricing(countryCode, pricing));
    }
}
