package org.mrozak.infrastructure.boundaries.outbound.api.adapters.track;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mrozak.domain.track.model.Track;

@Mapper(componentModel = "spring")
interface StatusResponseMapper {

    @Mapping(source = "statusResponse", target = "status")
    Track toDomain(int orderNumber, StatusResponse statusResponse);
}
