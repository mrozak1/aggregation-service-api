package org.mrozak.infrastructure.boundaries.outbound.api.adapters.shipment;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mrozak.domain.shipment.model.Shipment;

import java.util.List;

@Mapper(componentModel = "spring")
interface ProductResponseMapper {

    @Mapping(source = "productResponses", target = "products")
    Shipment toDomain(int orderNumber, List<ProductResponse> productResponses);
}
