package org.mrozak.infrastructure.boundaries.outbound.api.adapters.shipment;

import org.mrozak.domain.shipment.model.Shipment;
import org.mrozak.domain.shipment.ports.ShipmentApiPort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientRequestException;
import org.springframework.web.reactive.function.client.WebClientResponseException;

import io.netty.handler.timeout.ReadTimeoutException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Slf4j
@Component
@RequiredArgsConstructor
class ShipmentApiAdapter implements ShipmentApiPort {

    private final WebClient backendServicesWebClient;
    private final ProductResponseMapper productResponseMapper;

    @Override
    public Mono<Shipment> getShipment(final int orderNumber) {
        return backendServicesWebClient
                .get()
                .uri(builder -> builder
                        .path("/shipment-products")
                        .queryParam("orderNumber", orderNumber)
                        .build())
                .retrieve()
                .bodyToFlux(ProductResponse.class)
                .onErrorResume(WebClientResponseException.class, exception -> {
                    if (exception.getStatusCode() == HttpStatus.SERVICE_UNAVAILABLE) {
                        log.warn("Shipment service returned 503 for order number [{}].", orderNumber, exception);
                        return Flux.empty();
                    }
                    return Flux.error(exception);
                })
                .onErrorResume(WebClientRequestException.class, exception -> {
                    if (exception.getCause() instanceof ReadTimeoutException) {
                        log.warn("Shipment service request timeout for order number [{}].", orderNumber, exception);
                        return Mono.empty();
                    }
                    return Mono.error(exception);
                })
                .collectList()
                .map(products -> productResponseMapper.toDomain(orderNumber, products));
    }
}
