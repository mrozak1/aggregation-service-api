package org.mrozak.infrastructure.boundaries.outbound.api.adapters.shipment;

enum ProductResponse {
    ENVELOPE,
    BOX,
    PALLET
}
