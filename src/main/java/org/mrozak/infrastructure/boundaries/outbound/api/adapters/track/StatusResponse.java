package org.mrozak.infrastructure.boundaries.outbound.api.adapters.track;

enum StatusResponse {
    NEW,
    IN_TRANSIT,
    COLLECTING,
    COLLECTED,
    DELIVERING,
    DELIVERED
}
