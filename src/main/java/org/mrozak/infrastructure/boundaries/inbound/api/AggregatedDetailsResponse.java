package org.mrozak.infrastructure.boundaries.inbound.api;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

record AggregatedDetailsResponse(
        Map<String, List<String>> shipments,
        Map<String, String> track,
        Map<String, BigDecimal> pricing
) {
}
