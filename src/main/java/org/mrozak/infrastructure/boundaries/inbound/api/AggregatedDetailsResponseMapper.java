package org.mrozak.infrastructure.boundaries.inbound.api;

import org.mrozak.domain.AggregatedDetails;
import org.mrozak.domain.pricing.model.Pricing;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toMap;

@Component
class AggregatedDetailsResponseMapper {

    AggregatedDetailsResponse toResponse(final AggregatedDetails aggregatedDetails) {
        final Map<String, List<String>> shipments = getShipments(aggregatedDetails);
        final Map<String, String> tracks = getTracks(aggregatedDetails);
        final Map<String, BigDecimal> pricing = getPricing(aggregatedDetails);
        return new AggregatedDetailsResponse(shipments, tracks, pricing);
    }

    private Map<String, List<String>> getShipments(final AggregatedDetails aggregatedDetails) {
        return aggregatedDetails
                .shipments()
                .stream()
                .filter(shipment -> !shipment.products().isEmpty())
                .collect(toMap(
                        shipment -> Integer.toString(shipment.orderNumber()),
                        shipment -> shipment.products().stream().map(Enum::toString).toList()));
    }

    private Map<String, String> getTracks(final AggregatedDetails aggregatedDetails) {
        return aggregatedDetails
                .tracks()
                .stream()
                .collect(toMap(
                        track -> Integer.toString(track.orderNumber()),
                        track -> track.status().toString()));
    }

    private Map<String, BigDecimal> getPricing(final AggregatedDetails aggregatedDetails) {
        return aggregatedDetails
                .pricing()
                .stream()
                .collect(toMap(Pricing::countryCode, Pricing::amount));
    }
}
