package org.mrozak.infrastructure.boundaries.inbound.api;

import org.mrozak.domain.AggregatedDetailsFacade;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.Optional;
import java.util.Set;

import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Mono;

import static org.mrozak.infrastructure.boundaries.inbound.api.AggregatedDetailsEndpoint.AGGREGATION_ENDPOINT;

@RestController
@RequestMapping(AGGREGATION_ENDPOINT)
@RequiredArgsConstructor
class AggregatedDetailsEndpoint {

    static final String AGGREGATION_ENDPOINT = "/aggregation";
    static final String SHIPMENTS_ORDER_NUMBERS_QUERY_PARAM = "shipmentsOrderNumbers";
    static final String TRACK_ORDER_NUMBERS_QUERY_PARAM = "trackOrderNumbers";
    static final String PRICING_COUNTRY_CODES_QUERY_PARAM = "pricingCountryCodes";

    private static final Set<Integer> EMPTY_SHIPMENTS = Collections.emptySet();
    private static final Set<Integer> EMPTY_TRACKS = Collections.emptySet();
    private static final Set<String> EMPTY_PRICING = Collections.emptySet();

    private final AggregatedDetailsFacade aggregatedDetailsFacade;
    private final AggregatedDetailsResponseMapper aggregatedDetailsResponseMapper;

    @GetMapping
    Mono<AggregatedDetailsResponse> aggregate(
            @RequestParam(SHIPMENTS_ORDER_NUMBERS_QUERY_PARAM) Optional<Set<Integer>> shipmentsOrderNumbers,
            @RequestParam(TRACK_ORDER_NUMBERS_QUERY_PARAM) Optional<Set<Integer>> trackOrderNumbers,
            @RequestParam(PRICING_COUNTRY_CODES_QUERY_PARAM) Optional<Set<String>> pricingCountryCodes) {
        return aggregatedDetailsFacade
                .aggregate(
                        shipmentsOrderNumbers.orElse(EMPTY_SHIPMENTS),
                        trackOrderNumbers.orElse(EMPTY_TRACKS),
                        pricingCountryCodes.orElse(EMPTY_PRICING)
                )
                .map(aggregatedDetailsResponseMapper::toResponse);
    }
}
