package org.mrozak.infrastructure.config;

import org.mrozak.domain.AggregatedDetailsFacade;
import org.mrozak.domain.pricing.ports.PricingApiPort;
import org.mrozak.domain.shipment.ports.ShipmentApiPort;
import org.mrozak.domain.track.ports.TrackApiPort;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class DomainConfig {

    @Bean
    AggregatedDetailsFacade aggregatedDetailsFacade(
            final ShipmentApiPort shipmentApiPort,
            final TrackApiPort trackApiPort,
            final PricingApiPort pricingApiPort) {
        return new AggregatedDetailsFacade(shipmentApiPort, trackApiPort, pricingApiPort);
    }
}
