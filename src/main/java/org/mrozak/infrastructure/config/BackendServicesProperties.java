package org.mrozak.infrastructure.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.time.Duration;

@ConfigurationProperties("backend-services")
record BackendServicesProperties(Duration responseTimeout, String url) {
}
