package org.mrozak.infrastructure.config;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.reactive.function.client.WebClient;

import lombok.RequiredArgsConstructor;
import reactor.netty.http.client.HttpClient;

@Configuration
@RequiredArgsConstructor
@EnableConfigurationProperties(BackendServicesProperties.class)
class BackendServicesConfig {

    private final BackendServicesProperties backendServicesProperties;

    @Bean
    WebClient backendServicesWebClient() {
        final HttpClient httpClient = HttpClient.create().responseTimeout(backendServicesProperties.responseTimeout());
        return WebClient.builder()
                .baseUrl(backendServicesProperties.url())
                .clientConnector(new ReactorClientHttpConnector(httpClient))
                .build();
    }
}
