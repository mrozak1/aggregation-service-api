package org.mrozak.domain;

import org.mrozak.domain.pricing.model.Pricing;
import org.mrozak.domain.shipment.model.Shipment;
import org.mrozak.domain.track.model.Track;

import java.util.List;

public record AggregatedDetails(List<Shipment> shipments, List<Track> tracks, List<Pricing> pricing) {
}
