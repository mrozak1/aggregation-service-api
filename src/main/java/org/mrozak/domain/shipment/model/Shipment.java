package org.mrozak.domain.shipment.model;

import java.util.List;

public record Shipment(int orderNumber, List<Product> products) {

    public enum Product {
        ENVELOPE,
        BOX,
        PALLET
    }
}
