package org.mrozak.domain.shipment.ports;

import org.mrozak.domain.shipment.model.Shipment;

import reactor.core.publisher.Mono;

public interface ShipmentApiPort {
    Mono<Shipment> getShipment(int orderNumber);
}
