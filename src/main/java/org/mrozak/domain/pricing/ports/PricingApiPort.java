package org.mrozak.domain.pricing.ports;

import org.mrozak.domain.pricing.model.Pricing;

import reactor.core.publisher.Mono;

public interface PricingApiPort {
    Mono<Pricing> getPricing(String countryCode);
}
