package org.mrozak.domain.pricing.model;

import java.math.BigDecimal;

public record Pricing(String countryCode, BigDecimal amount) {
}
