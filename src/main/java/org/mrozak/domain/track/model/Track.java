package org.mrozak.domain.track.model;

public record Track(int orderNumber, Status status) {

    public enum Status {
        NEW,
        IN_TRANSIT,
        COLLECTING,
        COLLECTED,
        DELIVERING,
        DELIVERED
    }
}
