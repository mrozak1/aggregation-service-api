package org.mrozak.domain.track.ports;

import org.mrozak.domain.track.model.Track;

import reactor.core.publisher.Mono;

public interface TrackApiPort {
    Mono<Track> getTrack(int orderNumber);
}
