package org.mrozak.domain;

import org.mrozak.domain.pricing.model.Pricing;
import org.mrozak.domain.pricing.ports.PricingApiPort;
import org.mrozak.domain.shipment.model.Shipment;
import org.mrozak.domain.shipment.ports.ShipmentApiPort;
import org.mrozak.domain.track.model.Track;
import org.mrozak.domain.track.ports.TrackApiPort;
import org.reactivestreams.Publisher;

import java.util.List;
import java.util.Set;
import java.util.function.Function;

import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RequiredArgsConstructor
public class AggregatedDetailsFacade {

    private final ShipmentApiPort shipmentApiPort;
    private final TrackApiPort trackApiPort;
    private final PricingApiPort pricingApiPort;

    public Mono<AggregatedDetails> aggregate(
            final Set<Integer> shipmentOrderNumbers,
            final Set<Integer> trackOrderNumbers,
            final Set<String> countryCodes) {

        final Mono<List<Shipment>> shipments = getDetails(shipmentOrderNumbers, shipmentApiPort::getShipment);
        final Mono<List<Track>> tracks = getDetails(trackOrderNumbers, trackApiPort::getTrack);
        final Mono<List<Pricing>> pricing = getDetails(countryCodes, pricingApiPort::getPricing);

        return Mono.zip(shipments, tracks, pricing)
                .map(details -> new AggregatedDetails(details.getT1(), details.getT2(), details.getT3()));
    }

    private <A, B> Mono<List<B>> getDetails(final Set<A> elements, final Function<A, ? extends Publisher<? extends B>> mapper) {
        return Flux.fromIterable(elements).flatMap(mapper).collectList();
    }
}
